" Neovim general settings --------------------------------------------------------------------------

syntax enable

set background=dark
set clipboard=unnamedplus
set cmdheight=2

set colorcolumn=100
hi ColorColumn ctermbg=lightgrey

set encoding=utf-8
set fileencoding=utf-8
set mouse=a
set nobackup
set nofoldenable
set nowrap
set nowritebackup
set number
set ruler

" Indentation settings -----------------------------------------------------------------------------

set autoindent
set expandtab
set shiftround
set shiftwidth=4
set smartindent
set softtabstop=4
set tabstop=4

" EOF ----------------------------------------------------------------------------------------------
