# .bash_profile

# Aliases and functions ----------------------------------------------------------------------------

if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# Program initialization ---------------------------------------------------------------------------

if [ "$(tty)" = "/dev/tty1" ]; then                         # Launch Sway on login.
	exec sway
fi

# User-specific environment variables --------------------------------------------------------------

export EDITOR="nvim"                                        # Set default editor to Neovim.
export MOZ_ENABLE_WAYLAND=1                                 # Enable Wayland for Firefox.

# EOF ----------------------------------------------------------------------------------------------
