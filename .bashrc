# .bashrc

# Bash initialization ------------------------------------------------------------------------------

if [ -f /etc/bashrc ]; then                                         # Source global definitions.
	. /etc/bashrc
fi

if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]; then           # User specific environment.
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi

if [ -f /usr/share/nnn/quitcd/quitcd.bash_zsh ]; then
    source /usr/share/nnn/quitcd/quitcd.bash_zsh
fi

export PATH

# Aliases ------------------------------------------------------------------------------------------

# System-agnostic aliases

alias mv='mv -i'                                                    # Interactive move.

alias rm='rm -i'                                                    # Interactive remove.

# Distribution-specific aliases

alias min='sudo dnf in --setopt=install_weak_deps=False'            # Install packages without weak
                                                                    # dependencies.
alias rec='dnf rq -q --recommends'                                  # List recommended packages.

alias req='dnf rq -q --requires'                                    # List required packages.

# System-specific aliases

alias cc='clang -O2 *.c -o'                                         # Compile C with Clang.

alias cg='gcc -O2 -W *.c -o'                                        # Compile C with GCC.

alias cppc='clang++ -O2 *.cpp -o'                                   # Compile C++ with Clang.

alias cppg='g++ -O2 -W *.cpp -o'                                    # Compile C++ with GCC.

alias ls='n -de'                                                    # Launch NNN.

alias mc='~/Desktop/MultiMC/MultiMC'                                # Launch MultiMC.

alias mpa='mpv --vid=no'                                            # Stream audio.

alias red='wlsunset -t 3000 -T 6500 -l 35 -L -120'                  # Enable blue light filter.

alias vim='nvim'                                                    # Launch Neovim.

# Functions ----------------------------------------------------------------------------------------

comp() {                                                            # Compile a program, run it,
    printf "Testing\n"                                              # and remove it.
    
    if [[ $1 == "cc" ]]; then
        cc obj
    elif [[ $1 == "cg" ]]; then
        cg obj
    elif [[ $1 == "cppc" ]]; then
        cppc obj
    elif [[ $1 == "cppg" ]]; then
        cppg obj
    fi

    ./obj
    rm obj

    printf "Done testing\n"
}

musb() {                                                            # Mount USB.
    sudo mount /dev/$1 /media/usb
}

n()
{
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    nnn -de "$@"

    if [ -f "$NNN_TMPFILE" ]; then
        . "$NNN_TMPFILE"
        rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

rusb() {                                                            # Unmount USB.
    sudo umount /media/usb
}
    
# EOF ----------------------------------------------------------------------------------------------
